/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recitation2;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;

/**
 *
 * @author Richard
 */
public class Recitation2 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        Button Nbtn=new Button();
        btn.setText("Say 'Hello World'");
        Nbtn.setText("Say 'Goodbye Cruel World!'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        Nbtn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Goodbye Cruel World!");
            }
        });
        
        StackPane root = new StackPane();
        HBox thing= new HBox(30);
        thing.getChildren().add(btn);
        thing.getChildren().add(Nbtn);
        root.getChildren().add(thing);
        
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
